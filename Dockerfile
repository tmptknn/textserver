FROM alpine:3.12.7
RUN apk update
RUN apk upgrade
RUN apk add --update nodejs nodejs-npm
RUN rm -rf /var/cache/apk/*
COPY src/package.json package.json
COPY src/server.js server.js
RUN mkdir html
COPY html/index.html html/index.html
COPY html/textclient.js html/textclient.js
RUN npm install
EXPOSE 8081 1338 57332

CMD ["npm","start"]
