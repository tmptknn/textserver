const textinput = document.getElementById('textinput');
const button = document.getElementById('sendbutton');
const message = document.getElementById('messages');

button.onclick = function (){
  socket.send(JSON.stringify({message:textinput.value}));
};

let socket;

const ho = location.host.split(":")[0];

socket = new WebSocket("ws://"+(ho?ho:"localhost")+":1338");

socket.onopen = function() {
   console.log("connection open");
};

socket.onmessage = function (message) {

   let json;
   //console.log(""+message.data);
   try {
   json = JSON.parse(message.data);
   } catch (e) {
     console.log('This doesn\'t look like a valid JSON: ',
         message.data);
     return;
   }
   if(json){
     //if(json.yourId !== undefined){
     if(json.message !== undefined){
      console.log("Got message "+json.message)
       let mese = document.createElement("p");
       mese.innerHTML = json.message;
       messages.appendChild(mese);
     } else {
       console.log(""+message.data);
     }
   }
};

socket.onclose = function() {

   // websocket is closed.
   console.log("Connection is closed...");
};
