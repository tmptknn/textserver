'use strict';

const express = require('express');

// Constants
const PORT = 8081;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.sendFile('/html/index.html');
});

app.get('/textclient.js', (req, res) => {
  res.sendFile('/html/textclient.js');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response) {
});
server.listen(1338, function() { });


var listenserver = http.createServer(function(request, response) {
});
listenserver.listen(57332, function() { });

// create the server
const wsServer = new WebSocketServer({
  httpServer: server
});

const listenWsServer = new WebSocketServer({
  httpServer: listenserver
});

const clients = [];
let listenclient = null;
let clientId = 0;
const maxclients = 10;

function sendMessage(message){
  const data = JSON.stringify({message:message });
  for(let i=0; i<maxclients; i++) {
    if(clients[i]) clients[i].connection.sendUTF(data);
  }
  if(listenclient) listenclient.connection.sendUTF(data);
}

// WebSocket server
wsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin);
  let i=0;
  let found =false;
  while(i< maxclients && !found){
    if(clients[i]){
        console.log("id "+i+" reserved");
    }
    else{
      clientId = i;
      found =true;
    }
    if(i==maxclients-1){
      clientId =i;
      console.log("We are full kicking last one out");
    }
    i++;
  }
  const id = clientId;
  if(clients[id]){
    console.log("Dropping old connection "+id);
    clients[id].connection.close();
  }

  clients[id] = {connection:connection};
  
  if(clientId >=maxclients) clientId = 0;
  connection.on('message', function(message) {
    let json;
    if (message.type === 'utf8') {
      try {
      json = JSON.parse(message.utf8Data);
      } catch (e) {
        console.log('This doesn\'t look like a valid JSON: ',
            message.utf8Data);
        return;
      }
      if(json){
        if(json.message !== undefined){
          sendMessage(json.message);
        }
      }
    }
  });

  connection.on('close', function(connection) {
    console.log("bye "+id);
    clients[id] = null;
  });
});


listenWsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin);
  const id = 9;
  if(listenclient){
    console.log("Dropping old connection "+id);
    listenclient.connection.close();
  }
  listenclient = {connection:connection};
  connection.on('message', function(message) {
    console.log("Listener send message??!")
  });

  connection.on('close', function(connection) {
    console.log("bye listener "+id);
  });
});
